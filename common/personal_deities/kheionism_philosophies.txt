kheio_adm_factions = {
	adm_tech_cost_modifier = -0.1
	
	allow = {
		religion = kheionism
		has_factions = yes
	}
	
	sprite = 29
}

kheio_adm_1 = {
	adm_advisor_cost = -0.2
	
	allow = {
		religion = kheionism
		NOT = { crown_land_share = 33 }
		has_factions = no
	}
	
	sprite = 29
}

kheio_adm_2 = {
	adm_tech_cost_modifier = -0.1
	
	allow = {
		religion = kheionism
		crown_land_share = 33
		NOT = { crown_land_share = 66 }
		has_factions = no
	}
	
	sprite = 29
}

kheio_adm_3 = {
	free_adm_policy = 1
	
	allow = {
		religion = kheionism
		crown_land_share = 66
		has_factions = no
	}
	
	sprite = 29
}

kheio_dip_factions = {
	dip_tech_cost_modifier = -0.1
	
	allow = {
		religion = kheionism
		has_factions = yes
	}
	
	sprite = 29
}

kheio_dip_1 = {
	dip_advisor_cost = -0.2
	
	allow = {
		religion = kheionism
		NOT = { crown_land_share = 33 }
		has_factions = no
	}
	
	sprite = 29
}

kheio_dip_2 = {
	dip_tech_cost_modifier = -0.1
	
	allow = {
		religion = kheionism
		crown_land_share = 33
		NOT = { crown_land_share = 66 }
		has_factions = no
	}
	
	sprite = 29
}

kheio_dip_3 = {
	free_dip_policy = 1
	
	allow = {
		religion = kheionism
		crown_land_share = 66
		has_factions = no
	}
	
	sprite = 29
}

kheio_mil_factions = {
	mil_tech_cost_modifier = -0.1
	
	allow = {
		religion = kheionism
		has_factions = yes
	}
	
	sprite = 29
}

kheio_mil_1 = {
	mil_advisor_cost = -0.2
	
	allow = {
		religion = kheionism
		NOT = { crown_land_share = 33 }
		has_factions = no
	}
	
	sprite = 29
}

kheio_mil_2 = {
	mil_tech_cost_modifier = -0.1
	
	allow = {
		religion = kheionism
		crown_land_share = 33
		NOT = { crown_land_share = 66 }
		has_factions = no
	}
	
	sprite = 29
}

kheio_mil_3 = {
	free_mil_policy = 1
	
	allow = {
		religion = kheionism
		crown_land_share = 66
		has_factions = no
	}
	
	sprite = 29
}

kheio_no_policy = {
	global_unrest = 1

	allow = {
		always = no
	}

	sprite = 29
}

kheio_laskaris_factions = {
	yearly_absolutism = 1
	max_absolutism = 15
	
	allow = {
		religion = kheionism
		has_factions = yes
	}
	
	sprite = 29
}

kheio_laskaris_1 = {
	yearly_absolutism = 0.5
	max_absolutism = 10
	
	allow = {
		religion = kheionism
		NOT = { crown_land_share = 33 }
		has_factions = no
		tag = G00
	}
}

kheio_laskaris_2 = {
	yearly_absolutism = 1
	max_absolutism = 15
	
	allow = {
		religion = kheionism
		crown_land_share = 33
		NOT = { crown_land_share = 66 }
		has_factions = no
		tag = G00
	}
}

kheio_laskaris_3 = {
	yearly_absolutism = 1.5
	max_absolutism = 20
	
	allow = {
		religion = kheionism
		crown_land_share = 66
		has_factions = no
		tag = G00
	}
}