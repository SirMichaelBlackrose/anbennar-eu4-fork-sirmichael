runefather_maw = {

	# province it starts in
	# start =
		
	# date built in real life (so anything built during game time will be there if you start a game after that date)
	date = 1890.06.20

	#time to build
	time = {
		months = 1
	}

	#how much to build one
	build_cost = 250

	#can we move it?
	can_be_moved = no

	#tier that the project starts at when first placed in the game (be that at game start or when built by a country as the game progresses)
	starting_tier = 0

	#project type
	type = canal

	#can we build it?
	build_trigger = {
		has_province_flag = runefather_maw
	}

	#what to do when it's built
	on_built = {
	}

	#what to do when it's destroyed
	on_destroyed = {
	}
	
	keep_trigger = {
	}

	#can our country use it?
	can_use_modifiers_trigger = {
		religion = runefather_worship
		has_owner_religion = yes
		is_core = ROOT
	}

	#can our country upgrade it?
	can_upgrade_trigger = {
		religion = runefather_worship
		has_owner_religion = yes
		is_core = ROOT
	}

	#can our country keep it or is it destroyed when we get hold of it?
	# keep_trigger = {
	# }

	#tier data
	tier_0 = {
		#time to upgrade to this level (0 for tier 0)
		upgrade_time = {
			months = 0
		}

		#cost to upgrade to this level (0 for tier 0)
		cost_to_upgrade = {
			factor = 0
		}

		#what modifiers are added to the province when we have this project here on this tier
		province_modifiers = {
		}

		#what modifiers are added to the provinces in the map area when we have this project here on this tier
		area_modifier = {
		}

		#what modifiers are added to the country when we have this project here on this tier
		country_modifiers = {

		}

		#what effects happen when this tier is achieved
		on_upgraded = {
		}
	}

	tier_1 = {
		#time to upgrade to this level (0 for tier 0)
		upgrade_time = {
			months = 120
		}

		#cost to upgrade to this level (0 for tier 0)
		cost_to_upgrade = {
			factor = 7500
		}

		#what modifiers are added to the province when we have this project here on this tier
		province_modifiers = {
		}

		#what modifiers are added to the provinces in the map area when we have this project here on this tier
		area_modifier = {
		}

		#what modifiers are added to the country when we have this project here on this tier
		country_modifiers = {
			devotion = 0.5
		}

		#what effects happen when this tier is achieved
		on_upgraded = {
		}
	}

	tier_2 = {
		#time to upgrade to this level (0 for tier 0)
		upgrade_time = {
			months = 120
		}

		#cost to upgrade to this level (0 for tier 0)
		cost_to_upgrade = {
			factor = 15000
		}

		#what modifiers are added to the province when we have this project here on this tier
		province_modifiers = {
		}

		#what modifiers are added to the provinces in the map area when we have this project here on this tier
		area_modifier = {
		}

		#what modifiers are added to the country when we have this project here on this tier
		country_modifiers = {
			devotion = 1
			monthly_splendor = 0.5
		}

		#what effects happen when this tier is achieved
		on_upgraded = {
		}
	}

	tier_3 = {
		#time to upgrade to this level (0 for tier 0)
		upgrade_time = {
			months = 120
		}

		#cost to upgrade to this level (0 for tier 0)
		cost_to_upgrade = {
			factor = 25000
		}

		#what modifiers are added to the province when we have this project here on this tier
		province_modifiers = {
		}

		#what modifiers are added to the provinces in the map area when we have this project here on this tier
		area_modifier = {
		}

		#what modifiers are added to the country when we have this project here on this tier
		country_modifiers = {
			devotion = 1.5
			monthly_splendor = 1
			prestige = 1
		}

		#what effects happen when this tier is achieved
		on_upgraded = {
		}
	}
} 

#hul-az-krakazol MT
dd_stramolgiv = {
	# province it starts in
	start = 4289
	
	# date built in real life (so anything built during game time will be there if you start a game after that date)
	date = 1890.06.20

	#time to build
	time = { months = 12 }

	#how much to build one
	build_cost = 1

	#can we move it?
	can_be_moved = no

	#tier that the project starts at when first placed in the game (be that at game start or when built by a country as the game progresses)
	starting_tier = 0

	#project type
	type = monument

	#can we build it?
	build_trigger = { has_province_flag = krakazol_stramolgiv_found }

	#what to do when it's built
	on_built = {}

	#what to do when it's destroyed
	on_destroyed = {}

	#can our country use it?
	can_use_modifiers_trigger = { owner = { tag = I20 } }

	#can our country upgrade it?
	can_upgrade_trigger = { always = no }

	#can our country keep it or is it destroyed when we get hold of it?
	keep_trigger = { always = yes }

	#tier data
	tier_0 = {
		#time to upgrade to this level (0 for tier 0)
		upgrade_time = { months = 0 }

		#cost to upgrade to this level (0 for tier 0)
		cost_to_upgrade = { factor = 0 }

		#what modifiers are added to the province when we have this project here on this tier
		province_modifiers = {}

		#what modifiers are added to the provinces in the map area when we have this project here on this tier
		area_modifier = {}

		#what modifiers are added to the country when we have this project here on this tier
		country_modifiers = {}

		#what effects happen when this tier is achieved province_scope
		on_upgraded = {}
	}

	tier_1 = {
		#time to upgrade to this level (0 for tier 0)
		upgrade_time = { months = 0 }

		#cost to upgrade to this level (0 for tier 0)
		cost_to_upgrade = { factor = 0 }

		#what modifiers are added to the province when we have this project here on this tier
		province_modifiers = {
			local_development_cost = -0.4
			trade_value_modifier = 0.15
		}

		#what modifiers are added to the provinces in the map area when we have this project here on this tier
		area_modifier = {}

		#what modifiers are added to the country when we have this project here on this tier
		country_modifiers = {}

		#what effects happen when this tier is achieved
		on_upgraded = {
			change_trade_goods = wine
		}
	}

	tier_2 = {
		#time to upgrade to this level (0 for tier 0)
		upgrade_time = { months = 0 }

		#cost to upgrade to this level (0 for tier 0)
		cost_to_upgrade = { factor = 0 }

		#what modifiers are added to the province when we have this project here on this tier
		province_modifiers = {
			local_development_cost = -0.8
			trade_value_modifier = 0.3
			monthly_splendor = 0.25
		}

		#what modifiers are added to the provinces in the map area when we have this project here on this tier
		area_modifier = {}

		#what modifiers are added to the country when we have this project here on this tier
		country_modifiers = {}

		#what effects happen when this tier is achieved
		on_upgraded = {}
	}

	tier_3 = {
		#time to upgrade to this level (0 for tier 0)
		upgrade_time = { months = 0 }

		#cost to upgrade to this level (0 for tier 0)
		cost_to_upgrade = { factor = 0 }

		#what modifiers are added to the province when we have this project here on this tier
		province_modifiers = {
			local_development_cost = -1.2
			trade_value_modifier = 0.5
			monthly_splendor = 0.5
		}

		#what modifiers are added to the provinces in the map area when we have this project here on this tier
		area_modifier = {}

		#what modifiers are added to the country when we have this project here on this tier
		country_modifiers = {}

		#what effects happen when this tier is achieved
		on_upgraded = {}
	}
}


###################################
######## DWAROVAR DUNGEONS ########
###################################

# Frogarsalforn, library of the ancients
dd_frogarsalforn = {
	# start = 8

	date = 1.1.1

	time = {
		months = 0
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 1

	# project type
	type = monument

	build_trigger = {
		has_province_flag = dd_frogarsalforn_flag
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		always = yes
	}

	can_upgrade_trigger = {
		custom_trigger_tooltip = {
			tooltip = dd_upgrade_dungeon_tooltip
			has_province_flag = floor_explored
		}
	}

	keep_trigger = {
	}

	# tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 12
		}

		cost_to_upgrade = {
			factor = 500
		}

		province_modifiers = {
			trade_goods_size = 1
		}

		area_modifier = {
			trade_goods_size_modifier = 0.05
		}

		country_modifiers = {
		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 24
		}

		
		cost_to_upgrade = {
			factor = 750
		}

		
		province_modifiers = {
			trade_goods_size = 2
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.15
		}


		country_modifiers = {
		}

		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_3 = {
		upgrade_time = {
			months = 36
		}

		cost_to_upgrade = {
			factor = 1000
		}

		
		province_modifiers = {
			trade_goods_size = 3
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.25
		}

		
		country_modifiers = {
		}
		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
				clr_province_flag = explorable_dungeon
			}
		}
	}
}

#Stunveigry�rth, petrified forest
dd_stunveigryorth = {
	# start = 8

	date = 1.1.1

	time = {
		months = 0
	}
	
	build_cost = 0
	
	can_be_moved = no
	
	starting_tier = 1
	
	# project type
	type = monument
	
	build_trigger = {
		has_province_flag = dd_stunveigryorth_flag
	}
	
	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		always = yes
	}

	can_upgrade_trigger = {
		custom_trigger_tooltip = {
			tooltip = dd_upgrade_dungeon_tooltip
			has_province_flag = floor_explored
		}
	}

	keep_trigger = {}

	# tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 12
		}

		cost_to_upgrade = {
			factor = 500
		}

		province_modifiers = {
			trade_goods_size = 1
		}

		area_modifier = {
			trade_goods_size_modifier = 0.05
		}

		country_modifiers = {
		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 24
		}

		
		cost_to_upgrade = {
			factor = 750
		}

		
		province_modifiers = {
			trade_goods_size = 2
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.15
		}


		country_modifiers = {
		}

		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 36
		}

		
		cost_to_upgrade = {
			factor = 1000
		}

		
		province_modifiers = {
			trade_goods_size = 3
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.25
		}

		
		country_modifiers = {
		}
		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
				clr_province_flag = explorable_dungeon
			}
		}
	}
}

#Wytlramvar, Roots of the Spine
dd_wytlramvar = {
	# start = 8

	date = 1.1.1

	time = {
		months = 0
	}
	
	build_cost = 0
	
	can_be_moved = no
	
	starting_tier = 1
	
	# project type
	type = monument
	
	build_trigger = {
		has_province_flag = dd_wytlramvar_flag
	}
	
	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		always = yes
	}

	can_upgrade_trigger = {
		custom_trigger_tooltip = {
			tooltip = dd_upgrade_dungeon_tooltip
			has_province_flag = floor_explored
		}
	}

	keep_trigger = {}

	# tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 12
		}

		cost_to_upgrade = {
			factor = 500
		}

		province_modifiers = {
			trade_goods_size = 1
		}

		area_modifier = {
			trade_goods_size_modifier = 0.05
		}

		country_modifiers = {
		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 24
		}

		
		cost_to_upgrade = {
			factor = 750
		}

		
		province_modifiers = {
			trade_goods_size = 2
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.15
		}


		country_modifiers = {
		}

		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 36
		}

		
		cost_to_upgrade = {
			factor = 1000
		}

		
		province_modifiers = {
			trade_goods_size = 3
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.25
		}

		
		country_modifiers = {
		}
		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
				clr_province_flag = explorable_dungeon
			}
		}
	}
}

#Dimlherd, Geothermal Facility
dd_dimlherd = {
	# start = 8

	date = 1.1.1

	time = {
		months = 0
	}
	
	build_cost = 0
	
	can_be_moved = no
	
	starting_tier = 1
	
	# project type
	type = monument
	
	build_trigger = {
		has_province_flag = dd_dimlherd_flag
	}
	
	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		always = yes
	}

	can_upgrade_trigger = {
		custom_trigger_tooltip = {
			tooltip = dd_upgrade_dungeon_tooltip
			has_province_flag = floor_explored
		}
	}

	keep_trigger = {}

	# tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 12
		}

		cost_to_upgrade = {
			factor = 500
		}

		province_modifiers = {
			trade_goods_size = 1
		}

		area_modifier = {
			trade_goods_size_modifier = 0.05
		}

		country_modifiers = {
		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 24
		}

		
		cost_to_upgrade = {
			factor = 750
		}

		
		province_modifiers = {
			trade_goods_size = 2
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.15
		}


		country_modifiers = {
		}

		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 36
		}

		
		cost_to_upgrade = {
			factor = 1000
		}

		
		province_modifiers = {
			trade_goods_size = 3
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.25
		}

		
		country_modifiers = {
		}
		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
				clr_province_flag = explorable_dungeon
			}
		}
	}
}

#Grumhardh�m, Great Forge
dd_grumhardhum = {
	# start = 8

	date = 1.1.1

	time = {
		months = 0
	}
	
	build_cost = 0
	
	can_be_moved = no
	
	starting_tier = 1
	
	# project type
	type = monument
	
	build_trigger = {
		has_province_flag = dd_grumhardhum_flag
	}
	
	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		always = yes
	}

	can_upgrade_trigger = {
		custom_trigger_tooltip = {
			tooltip = dd_upgrade_dungeon_tooltip
			has_province_flag = floor_explored
		}
	}

	keep_trigger = {}

	# tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 12
		}

		cost_to_upgrade = {
			factor = 500
		}

		province_modifiers = {
			trade_goods_size = 1
		}

		area_modifier = {
			trade_goods_size_modifier = 0.05
		}

		country_modifiers = {
		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 24
		}

		
		cost_to_upgrade = {
			factor = 750
		}

		
		province_modifiers = {
			trade_goods_size = 2
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.15
		}


		country_modifiers = {
		}

		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 36
		}

		
		cost_to_upgrade = {
			factor = 1000
		}

		
		province_modifiers = {
			trade_goods_size = 3
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.25
		}

		
		country_modifiers = {
		}
		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
				clr_province_flag = explorable_dungeon
			}
		}
	}
}

#Leforn V�mbr�thar, Abandoned Workshop
dd_leforn = {
	# start = 8

	date = 1.1.1

	time = {
		months = 0
	}
	
	build_cost = 0
	
	can_be_moved = no
	
	starting_tier = 1
	
	# project type
	type = monument
	
	build_trigger = {
		has_province_flag = dd_leforn_flag
	}
	
	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		always = yes
	}

	can_upgrade_trigger = {
		custom_trigger_tooltip = {
			tooltip = dd_upgrade_dungeon_tooltip
			has_province_flag = floor_explored
		}
	}

	keep_trigger = {}

	# tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 12
		}

		cost_to_upgrade = {
			factor = 500
		}

		province_modifiers = {
			trade_goods_size = 1
		}

		area_modifier = {
			trade_goods_size_modifier = 0.05
		}

		country_modifiers = {
		}

		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 24
		}

		
		cost_to_upgrade = {
			factor = 750
		}

		
		province_modifiers = {
			trade_goods_size = 2
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.15
		}


		country_modifiers = {
		}

		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 36
		}

		
		cost_to_upgrade = {
			factor = 1000
		}

		
		province_modifiers = {
			trade_goods_size = 3
		}

		
		area_modifier = {
			trade_goods_size_modifier = 0.25
		}

		
		country_modifiers = {
		}
		
		on_upgraded = {
			hidden_effect = {
				clr_province_flag = floor_explored
				clr_province_flag = explorable_dungeon
			}
		}
	}
}